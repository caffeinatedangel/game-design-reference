﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace SpriteSheet {
	public class SpriteSheetExample : Game {
		GraphicsDeviceManager graphics;
		SpriteBatch spriteBatch;

		/*
		 * These two are just normal varaibles like you'd have for anything else.
		 * A texture2D storing the texture you want to draw (in our case the entire spritesheet)
		 * and a rectangle with the location (we're not actually ever changing it in this example)
		 */
		Texture2D mySS;
		readonly Rectangle mySpriteLocation = new Rectangle(0, 0, 165, 293);

		/*
		 * Here we have the three magic varaibles that make it all work.
		 * The first one (currentFrame) is the actual tracker for what frame we're on
		 * The second one (totalFrames) is the total number of frames in the sprite sheet
		 * The third one (framesPerRow) is the number of frames in each row 
		 * 		(you can work without this one if you're using a 1 dimentional sheet)
		 */
		Int32 currentFrame = 0;
		const Int32 totalFrames = 64;
		const Int32 framesPerRow = 12;

		public SpriteSheetExample() {
			graphics = new GraphicsDeviceManager(this);
			Content.RootDirectory = "Content";
		}

		protected override void LoadContent() {
			spriteBatch = new SpriteBatch(GraphicsDevice);
			mySS = Content.Load<Texture2D>("spriteSheet.png");
		}

		protected override void Update(GameTime gameTime) {
			if(Keyboard.GetState().IsKeyDown(Keys.Escape))
				Exit();
			/*
			 * The real magic of the program right here. Add one to the counter,
			 * and mod it by the total number of frames so it loops
			 */
			currentFrame = (currentFrame + 1) % totalFrames;
			base.Update(gameTime);
		}

		protected override void Draw(GameTime gameTime) {
			graphics.GraphicsDevice.Clear(Color.CornflowerBlue);
			spriteBatch.Begin();
			/*
			 * The ACTUAL real magic
			 * 
			 * The first two arguments, and the fourth are the same as normal. Your texture, and your target rectangle
			 * The third one is a little complicated. It's a rectangle of the position on the Texture to draw to that rectangle
			 * 		For the X Component of the rectangle, we take the current frame number, mod the number of frames per row,
			 * 			which gives us the frame number in the row. We then multiply that by the width of one frame
			 * 			(in this case also the width of our target, but if your target is a different size than the sprite,
			 * 			this will need to be taken into account)
			 * 		For the Y Component we need the row we're in, which is simply currentFrame divided by the number of frames in a row
			 * 			(dividing an integer by another integer automatically discards any fractional part)
			 * 		For the Width and Height, these are simply the same as our target.
			 * 
			 * 		XNA's API may require you to add additional arguments, however these may all be set to null, 0, or None depending on their type
			 */
			spriteBatch.Draw(mySS, mySpriteLocation, new Rectangle((currentFrame % framesPerRow) * mySpriteLocation.Width, (currentFrame / framesPerRow) * mySpriteLocation.Height, mySpriteLocation.Width, mySpriteLocation.Height), Color.White);
			spriteBatch.End();
			base.Draw(gameTime);
		}
	}
}

