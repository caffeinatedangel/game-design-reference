# Welcome #
Hello, and welcome to the Repository of cheat codes for Larry Foster's Game Design Simulator 2015.


In all seriousness, this is a (hopefully) useful set of examples and MWEs for XNA game design in general. Feel free to ask me if you want something else put up here, or if you can/feel like doing it yourself, fork this and make a pull request (you'll learn so much about git while doing so too!)

## A Word of Caution ##
Please note that these examples were made in MonoGame.

As such, while the API is incredibly similar, **these examples may not be 100% copypasta compatible**.
That said, MonoGame has very few differences, and these examples will not be making use of the extended feature set it offers, which means aside from small syntax differences occasionally, everything will be the same.